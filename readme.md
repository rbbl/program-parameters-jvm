# DEPRECATED: use [clikt options](https://ajalt.github.io/clikt/options/) instead

# Program Parameters JVM

this is a small Kotlin Library for the JVM that is used to load easily load Parameters from different Sources.

Sources:

- Program Arguments
- Properties File
- Environment Variables

## Usage

### Installation

#### gradle kotlin

```kotlin
repositories {
    maven("https://gitlab.com/api/v4/projects/28863171/packages/maven")
}

dependencies {
    implementation("cc.rbbl:program-parameters-jvm:1.0.3")
}
```

#### maven

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project>
    <repositories>
        <repository>
            <id>gitlab-maven</id>
            <url>https://gitlab.com/api/v4/projects/28863171/packages/maven</url>
        </repository>
    </repositories>

    <distributionManagement>
        <repository>
            <id>gitlab-maven</id>
            <url>https://gitlab.com/api/v4/projects/28863171/packages/maven</url>
        </repository>

        <snapshotRepository>
            <id>gitlab-maven</id>
            <url>https://gitlab.com/api/v4/projects/28863171/packages/maven</url>
        </snapshotRepository>
    </distributionManagement>

    <dependencies>
        <dependency>
            <groupId>cc.rbbl</groupId>
            <artifactId>program-parameters-jvm</artifactId>
            <version>1.0.3</version>
        </dependency>
    </dependencies>
</project>
```

### Usage

```kotlin
fun main(args: Array<String>) {
    val params = ParameterHolder(
        setOf(
            ParameterDefinition("DISCORD_TOKEN"),
            ParameterDefinition("SPOTIFY_CLIENT_ID", caseSensitive = false),
            ParameterDefinition("SPOTIFY_CLIENT_SECRET", caseSensitive = false, required = false)
        )
    )
    params.loadParametersFromEnvironmentVariables()
    params.loadParameters(args)
    params.checkParameterCompleteness()

    //...use the parameters in your Program
    params["DISCORD_TOKEN"] //gets the loaded param
}
```

