package cc.rbbl.program_parameters_jvm

/**
 *  A Description on how a Parameter should be loaded and if it's a required Parameter.
 *
 *  @param key the Key of the Parameter how it's expected in an ideal Case
 *  @param required whether the parameter is required when getting checked by the [ParameterHolder]
 *  @param caseSensitive if the input Key has to have the exact same casing
 *  @constructor creates a case-sensitive and required ParameterDefinition
 */
data class ParameterDefinition(val key: String, val required: Boolean = true, val caseSensitive: Boolean = true) {
    init {
        if (key.isEmpty()) {
            throw IllegalArgumentException("key can't be empty")
        }
    }

    /**
     * used to check if the Input matches the Key depending on case sensitivity setting.
     * @param comparisonKey input key to check
     */
    fun matches(comparisonKey: String): Boolean {
        return if (caseSensitive) {
            key == comparisonKey
        } else {
            key.equals(comparisonKey, true)
        }
    }
}