package cc.rbbl.program_parameters_jvm

import java.io.InputStream
import java.util.*

/**
 * Object to load parameters from different sources based on the passed [ParameterDefinition]s
 *
 * Any loading operations performed may override values loaded from previous loading operations
 *
 * @param parametersToLoad parameter definitions to be handled by this [ParameterHolder]
 * @constructor instantiates a [ParameterHolder]
 */
class ParameterHolder(private val parametersToLoad: Set<ParameterDefinition>) : Map<String, String> {

    /**
     * creates default [ParameterDefinition] and passes them to the primary constructor
     */
    constructor(parameterKeys: Array<String>) : this(parameterKeys.map { ParameterDefinition(it) }.toSet())

    private val parameterMap: MutableMap<String, String> = HashMap()

    init {
        val keySet = parametersToLoad.map { it.key }.toSet()
        val keyList = parametersToLoad.map { it.key }
        val tempKeyList = keyList as MutableList<String>
        for (key in keySet) {
            tempKeyList.removeAt(keyList.indexOf(key))
        }
        if (tempKeyList.isNotEmpty()) {
            throw IllegalArgumentException(
                tempKeyList.toSet().fold("Duplicate Keys found: ") { printList, element -> "$printList$element, " }
                    .trim()
                    .removeSuffix(",")
            )
        }
    }

    override val entries: Set<Map.Entry<String, String>>
        get() = parameterMap.entries
    override val keys: Set<String>
        get() = parameterMap.keys
    override val values: Collection<String>
        get() = parameterMap.values
    override val size: Int
        get() = parameterMap.size

    /**
     * loads parameters from the passed array
     *
     * @param args list of arguments as passed into a main method
     */
    fun loadParameters(args: Array<String>) {
        val set = HashSet<Pair<String, String>>()
        for (arg in args) {
            val keyValue = arg.split("=").toTypedArray()
            if (keyValue.size != 2) {
                continue
            }
            set.add(Pair(keyValue[0], keyValue[1]))
        }
        loadParameters(set)
    }

    /**
     * loads parameters from a properties file
     *
     * @param inputStream of a valid .properties file
     */
    fun loadParametersFromPropertiesFile(inputStream: InputStream) {
        val properties = Properties()
        properties.load(inputStream)
        loadParameters(properties.entries.map { Pair(it.key.toString(), it.value.toString()) }
            .toSet())
    }

    /**
     * loads the parameters from the system environment variables
     */
    fun loadParametersFromEnvironmentVariables() {
        loadParameters(System.getenv().map { Pair(it.key, it.value) }.toSet())
    }

    /**
     * loads parameters from a key value set - mainly used as base method for the other loading functions
     */
    fun loadParameters(input: Set<Pair<String, String>>) {
        for (pair in input) {
            for (parameter in parametersToLoad) {
                if (parameter.matches(pair.first)) {
                    parameterMap[parameter.key] = pair.second
                }
            }
        }
    }

    /**
     * checks if all the parameters are complete based on [ParameterDefinition.required]
     *
     * @throws [IllegalArgumentException] when at least one of the required parameters is missing
     */
    fun checkParameterCompleteness() {
        val missingParams = HashSet<String>()
        parametersToLoad.filter { it.required }.forEach {
            if (parameterMap[it.key].isNullOrBlank()) {
                missingParams.add(it.key)
            }
        }

        if (missingParams.isNotEmpty()) {
            throw IllegalArgumentException(missingParams.fold("Missing Parameters:") { total, element -> "$total $element," }
                .removeSuffix(","))
        }
    }

    override fun containsValue(value: String) = parameterMap.containsValue(value)

    override fun isEmpty() = parameterMap.isEmpty()

    override fun containsKey(key: String) = parameterMap.containsKey(key)

    override fun get(key: String) = parameterMap[key]

    operator fun get(key: ParameterDefinition): String? {
        return this[key.key]
    }
}