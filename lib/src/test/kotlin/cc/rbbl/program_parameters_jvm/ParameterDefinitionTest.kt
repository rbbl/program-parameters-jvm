package cc.rbbl.program_parameters_jvm

import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue

internal class ParameterDefinitionTest {

    @Test
    fun initializationTestEmptyKey() {
        assertThrows<IllegalArgumentException> { ParameterDefinition("") }
    }

    @Nested
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    inner class CaseSensitive {
        lateinit var testee: ParameterDefinition

        @BeforeAll
        fun prepareTests() {
            testee = ParameterDefinition("Test_Key")
        }

        @Test
        fun matchesTestPositive() {
            assertTrue(testee.matches("Test_Key"))
        }

        @Test
        fun matchesTestNegative() {
            assertFalse(testee.matches("Test_key"))
        }

        @Test
        fun matchesTestNegativeTwo() {
            assertFalse(testee.matches("Test_ke"))
        }
    }

    @Nested
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    inner class CaseInsensitive {
        lateinit var testee: ParameterDefinition

        @BeforeAll
        fun prepareTests() {
            testee = ParameterDefinition("Test_Key", caseSensitive = false)
        }

        @Test
        fun matchTestPositiveExact() {
            assertTrue(testee.matches("Test_Key"))
        }

        @Test
        fun matchesTestPositiveRough() {
            assertTrue(testee.matches("test_keY"))
        }

        @Test
        fun matchTestNegative() {
            assertFalse(testee.matches("Test_ke"))
        }
    }
}