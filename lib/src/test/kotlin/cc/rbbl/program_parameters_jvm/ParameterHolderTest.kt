package cc.rbbl.program_parameters_jvm

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import org.junit.jupiter.api.assertThrows
import uk.org.webcompere.systemstubs.environment.EnvironmentVariables

internal class ParameterHolderTest {
    @Test
    fun initializationTestPositive() {
        assertDoesNotThrow {
            ParameterHolder(
                setOf(
                    ParameterDefinition("Test_Key"),
                    ParameterDefinition("test_key"),
                    ParameterDefinition("Test_Key_two")
                )
            )
        }
    }

    @Test
    fun initializationTestNegative() {
        val exception = assertThrows<IllegalArgumentException> {
            ParameterHolder(
                setOf(
                    ParameterDefinition("Test_Key"),
                    ParameterDefinition("test_key"),
                    ParameterDefinition("Test_Key", caseSensitive = false),
                    ParameterDefinition("Test_Key_two"),
                    ParameterDefinition("Test_Key_two", caseSensitive = false),
                    ParameterDefinition("Test_Key_two", caseSensitive = false, required = false)
                )
            )
        }
        assertEquals("Duplicate Keys found: Test_Key, Test_Key_two", exception.message)
    }

    @Test
    fun loadParametersFromArgsTest() {
        val testee = ParameterHolder(
            setOf(
                ParameterDefinition("Test_Key"),
                ParameterDefinition("Test_Key_two", caseSensitive = false)
            )
        )
        val testParams = arrayOf("Test_Key=first", "test_key_TWO=second")
        testee.loadParameters(testParams)
        assertEquals("first", testee["Test_Key"])
        assertEquals("second", testee[ParameterDefinition("Test_Key_two")])
    }

    @Test
    fun loadParametersFromPropertiesTest() {
        val testee = ParameterHolder(
            setOf(
                ParameterDefinition("Test_Key"),
                ParameterDefinition("Test_Key_two.value", caseSensitive = false)
            )
        )
        testee.loadParametersFromPropertiesFile(
            this.javaClass.getResource("test.properties")!!.openStream()
        )
        assertEquals("first", testee["Test_Key"])
        assertEquals("second", testee[ParameterDefinition("Test_Key_two.value")])
    }

    @Test
    fun loadParametersFromPairSetTest() {
        val testee = ParameterHolder(
            setOf(
                ParameterDefinition("Test_Key"),
                ParameterDefinition("Test_Key_two", caseSensitive = false)
            )
        )
        testee.loadParameters(setOf(Pair("Test_Key", "first"), Pair("test_key_TWO", "second")))
        assertEquals("first", testee["Test_Key"])
        assertEquals("second", testee[ParameterDefinition("Test_Key_two")])
    }

    @Test
    fun loadParametersFromEnvTest() {
        val testee = ParameterHolder(
            setOf(
                ParameterDefinition("Test_Key"),
                ParameterDefinition("Test_Key_two", caseSensitive = false)
            )
        )
        EnvironmentVariables().set("Test_Key", "first").set("test_key_TWO", "second")
            .execute { testee.loadParametersFromEnvironmentVariables() }
        assertEquals("first", testee["Test_Key"])
        assertEquals("second", testee[ParameterDefinition("Test_Key_two")])
    }

    @Test
    fun checkParameterCompletenessTestNegative() {
        val testee = ParameterHolder(
            setOf(
                ParameterDefinition("Test_Key"),
                ParameterDefinition("Test_Key_two"),
                ParameterDefinition("Test_Key_three")
            )
        )
        testee.loadParameters(setOf(Pair("Test_Key", "first")))
        val exception = assertThrows<IllegalArgumentException> { testee.checkParameterCompleteness() }
        assertEquals("Missing Parameters: Test_Key_two, Test_Key_three", exception.message)
    }

    @Test
    fun checkParameterCompletenessTestNegativeEmpty() {
        val testee = ParameterHolder(
            setOf(
                ParameterDefinition("Test_Key"),
                ParameterDefinition("Test_Key_two"),
                ParameterDefinition("Test_Key_three")
            )
        )
        testee.loadParameters(setOf(Pair("Test_Key", "first"), Pair("Test_Key_two", "")))
        val exception = assertThrows<IllegalArgumentException> { testee.checkParameterCompleteness() }
        assertEquals("Missing Parameters: Test_Key_two, Test_Key_three", exception.message)
    }

    @Test
    fun checkParameterCompletenessTestPositive() {
        val testee = ParameterHolder(
            setOf(
                ParameterDefinition("Test_Key"),
                ParameterDefinition("Test_Key_two")
            )
        )
        testee.loadParameters(setOf(Pair("Test_Key", "first"), Pair("Test_Key_two", "second")))
        assertDoesNotThrow { testee.checkParameterCompleteness() }
    }

    @Test
    fun dottedParametersTest() {
        val testee = ParameterHolder(
            setOf(
                ParameterDefinition("mail.username"),
                ParameterDefinition("mail.password")
            )
        )
        EnvironmentVariables().set("mail.username", "username").set("mail.password", "aPassword")
            .execute { testee.loadParametersFromEnvironmentVariables() }
        assertEquals("username", testee["mail.username"])
        assertEquals("aPassword", testee[ParameterDefinition("mail.password")])
    }
}